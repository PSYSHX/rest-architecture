from flask import Blueprint
from flask_restx import Namespace, Resource
from flask_keycloak import KeycloakProtect

keycloak = KeycloakProtect()

patients_bp = Blueprint('patients', __name__, url_prefix='/patients')
api = Namespace('Patients', description='Operations related to patients')

@keycloak.protect()
@api.route('/')
class PatientsResource(Resource):
    def get(self):
        # Your secured endpoint logic
        return {'message': 'Secured endpoint - Patients List'}
