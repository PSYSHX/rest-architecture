from typing import List
from app.models import Patient

class PatientService:
    patients = []

    @staticmethod
    def create_patient(patient_data: dict) -> Patient:
        # Implement logic to create a new patient
        new_patient = Patient(**patient_data)
        PatientService.patients.append(new_patient)
        return new_patient

    @staticmethod
    def update_patient(patient_id: int, patient_data: dict) -> Patient:
        # Implement logic to update an existing patient
        patient = PatientService.get_patient(patient_id)
        for key, value in patient_data.items():
            setattr(patient, key, value)
        return patient

    @staticmethod
    def delete_patient(patient_id: int) -> None:
        # Implement logic to delete a patient
        PatientService.patients = [p for p in PatientService.patients if p.id != patient_id]

    @staticmethod
    def get_patient(patient_id: int) -> Patient:
        # Implement logic to retrieve a specific patient
        return next((p for p in PatientService.patients if p.id == patient_id), None)

    @staticmethod
    def list_patients() -> List[Patient]:
        # Implement logic to list all patients
        return PatientService.patients

    @staticmethod
    def search_patients(criteria: dict) -> List[Patient]:
        # Implement logic to search patients by criteria
        # For simplicity, assume criteria is a dictionary of key-value pairs
        return [p for p in PatientService.patients if all(getattr(p, key, None) == value for key, value in criteria.items())]

    @staticmethod
    def list_patients_with_pagination(page: int, per_page: int) -> List[Patient]:
        # Implement logic to list patients with pagination
        start_index = (page - 1) * per_page
        end_index = start_index + per_page
        return PatientService.patients[start_index:end_index]
