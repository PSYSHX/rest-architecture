class Patient:
    def __init__(self, id: int, name: str, birthdate: str, sex: str, ssn: str):
        self.id = id
        self.name = name
        self.birthdate = birthdate
        self.sex = sex
        self.ssn = ssn
