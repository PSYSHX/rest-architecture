from flask import Blueprint, jsonify, request
from app.services.patient_service import PatientService

patients_bp = Blueprint('patients', __name__)

@patients_bp.route('/', methods=['POST'])
def create_patient():
    patient_data = request.json
    new_patient = PatientService.create_patient(patient_data)
    return jsonify(new_patient), 201

@patients_bp.route('/<int:patient_id>', methods=['PUT'])
def update_patient(patient_id):
    patient_data = request.json
    updated_patient = PatientService.update_patient(patient_id, patient_data)
    return jsonify(updated_patient)

@patients_bp.route('/<int:patient_id>', methods=['DELETE'])
def delete_patient(patient_id):
    PatientService.delete_patient(patient_id)
    return '', 204

@patients_bp.route('/<int:patient_id>', methods=['GET'])
def get_patient(patient_id):
    patient = PatientService.get_patient(patient_id)
    return jsonify(patient)

@patients_bp.route('/', methods=['GET'])
def list_patients():
    patients = PatientService.list_patients()
    return jsonify(patients)

@patients_bp.route('/search', methods=['GET'])
def search_patients():
    criteria = request.args
    patients = PatientService.search_patients(criteria)
    return jsonify(patients)

@patients_bp.route('/paginate', methods=['GET'])
def list_patients_with_pagination():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 10, type=int)
    patients = PatientService.list_patients_with_pagination(page, per_page)
    return jsonify(patients)
