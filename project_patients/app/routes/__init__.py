from flask import Blueprint

patients_bp = Blueprint('patients', __name__)

from app.routes.patients import patients_bp
