from flask import Flask
from flask_restx import Api
from flask_keycloak import Keycloak

def create_app():
    app = Flask(__name__)

    # Load configuration from config.py
    app.config.from_object('app.config.Config')

    # Initialize Flask-RESTx
    api = Api(app, version='1.0', title='Patient API', description='API for managing patients')

    # Configure Keycloak
    keycloak = Keycloak(app)

    # Register the patients blueprint
    from app.routes.patients import patients_bp
    api.add_namespace(patients_bp)

    return app
